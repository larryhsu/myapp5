/**
 * 全局设置，用于普通模式构建下修改环境
 * 定制化：如果会场不需要某个字段，删除即可，专业模式构建也会删除这个选项
 * debug模式: 打开vconosle，开启所有接口的预发环境，使用预览
 */

module.exports = {
  mockjs: false, // 启用mockjs，默认不启用
  debug: false, // 启用debug，默认不启用
};
