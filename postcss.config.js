// 为不同的浏览器自动添加前缀
var autoprefixer = require("autoprefixer")

module.exports = {
  plugins: [
    autoprefixer({
      overrideBrowserslist: [
        "Android >= 4.0",
        "iOS >= 6",
        "last 5 QQAndroid versions",
        "last 5 UCAndroid versions"
      ],
      cascade: true
    })
  ]
}