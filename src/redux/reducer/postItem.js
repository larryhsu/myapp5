import { AcType } from '../action/index.js';

let initState = {
	item_list: [],
	search_list: [],
	hasData: true,
};

function PostItmes(state = initState, action) {
	switch (action.type) {
		case AcType.GET_POST_ITEMS:
			return {
				...state,
				item_list: [...state.item_list, ...action.data],
			};
		case AcType.GET_NONE:
			return {
				...state,
				hasData: false,
			};
		case AcType.SAVE_SEARCH_DATA:
			return {
				...state,
				search_list: action.data,
				hasData: action.hasData,
			};
		default:
			return state;
	}
}

export default PostItmes;
