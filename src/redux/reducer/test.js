import { AcType } from '../action/index'

let initState = {
  data: [1]
}

function Test(state=initState, action) {
  switch(action.type) {
    case AcType.ADD:
      return {
        ...state,
        data: [...state.data, 1]
      }
    default:
      return state
  }
}

export default Test