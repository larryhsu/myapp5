import { AcType } from '../action/index.js';

const initState = {
  data:[],
  hadData: true,
}

// 获取每日推荐内容
function GetRecommend(state = initState, action) {
  switch(action.type) {
    case AcType.GET_REC:
      return {
        ...state,
        data:[...state.data, ...action.data]
      }
    case AcType.GET_NONE:
    default:
      return state;
  }
}

export default GetRecommend