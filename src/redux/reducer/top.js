import { AcType } from '../action/index';

let initState = {
  top: 0
}

function Top(state = initState, action) {
  switch (action.type) {
    case AcType.SAVE_TOP: 
      return {
        ...initState,
        top: action.top
      }
    default:
      return state;
  }
}

export default Top;