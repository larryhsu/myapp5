import { combineReducers } from 'redux';
import postItem from './postItem.js';
import test from './test.js';
import recommend from './recommend.js';
import top from './top.js';
import sign from './sign.js';
import draftList from './draftList.js';

export default combineReducers({
	postItem,
	test,
	recommend,
	top,
	sign,
	draftList,
});
