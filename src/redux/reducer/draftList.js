import { AcType } from '../action/index.js';

let initState = {
	draft_list: [],
};

function DraftList(state = initState, action) {
	switch (action.type) {
		case AcType.GET_DRAFT_LIST:
			return {
				...state,
				draft_list: [...state.draft_list, ...action.data],
			};
		default:
			return state;
	}
}

export default DraftList;
