import { AcType } from '../action/index';

let initState = {
	isLogin: false,
	usr: null,
};

function Sign(state = initState, action) {
	switch (action.type) {
		case AcType.SIN_SUC:
			return {
				...state,
				isLogin: true,
				usr: action.data,
			};
		case AcType.SIN_OUT:
			return {
				...state,
				isLogin: false,
				usr: null,
			};
		case AcType.SIN_FAIL:
			return {
				...state,
				isLogin: false,
				usr: null,
			};
		default:
			return state;
	}
}

export default Sign;
