import { fetchData } from 'Common/methods';
import { AcType } from './index.js';

function getBlogList(url, method, opt = {}) {
	return (dispatch) => {
		fetchData(url, method, opt)
			.then((res) => {
				if (res.data.length) {
					dispatch({
						type: AcType.GET_POST_ITEMS,
						data: res.data,
					});
				} else {
					dispatch({ type: AcType.GET_NONE });
				}
			})
			.catch((err) => {
				console.log('fetch data error');
			});
	};
}

// 获取每日推荐内容
function getRecommend(url, method, opt = {}) {
	return (dispatch) => {
		fetchData(url, method, opt)
			.then((res) => {
				if (res.data.length) {
					dispatch({
						type: AcType.GET_REC,
						data: res.data,
					});
				} else {
					dispatch({ type: AcType.GET_NONE });
				}
			})
			.catch((err) => {
				console.log('fetch data error', err);
			});
	};
}

export { getBlogList, getRecommend };
