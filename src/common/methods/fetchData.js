import axios from "axios";

// 获取数据
function fetchData(url, method = 'get', option = {}) {
  return new Promise((resolve, reject) => {
    if (method === 'get') {
      axios.get(url)
        .then(res => resolve(res))
        .catch(err => reject(err))
    } else if (method === 'post') {
      axios.post(url, option)
        .then(res => {
          resolve(res);
        })
        .catch(err => reject(err))
    }
  });
}

export default fetchData;