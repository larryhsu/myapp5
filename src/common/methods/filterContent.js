const hashtag = /#/g;

function filterContent(content) {
  // TODO:过滤更多的Markdown标记符号
  content =  content.replace(hashtag, '').substring(0, 200);
  return content;
}

export default filterContent;