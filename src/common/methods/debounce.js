// 第一次就执行的防抖函数
function debounce(fn, wait) {
	var timer = null;
	var self = this;
	var args = arguments;
	var count = 0;
	return function () {
		clearTimeout(timer);
		if (count == 0) {
			// 第一次立即执行
			fn.apply(self, args);
			count++;
		} else {
			timer = setTimeout(function () {
				fn.apply(self, args);
				count++;
			}, wait);
		}
	};
}

export default debounce;
