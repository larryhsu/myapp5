const reCalcId = (uuid, auid) => {
	try {
		uuid = Number(uuid);
		auid = Number(auid);
		const uuid_a = 1;
		const uuid_b = 1286;
		const uuid_c = 100000 - uuid;

		// 解算一元二次方程
		const postId_a =
			((-uuid_b + Math.sqrt(uuid_b * uuid_b - 4 * uuid_a * uuid_c)) / 2) *
			uuid_a;
		const postId_b =
			((-uuid_b - Math.sqrt(uuid_b * uuid_b - 4 * uuid_a * uuid_c)) / 2) *
			uuid_a;

		const post_id = postId_a - postId_b > 0 ? postId_a : postId_b;

		return post_id;
	} catch (err) {
		return -1;
	}
};

export default reCalcId;
