import axios from 'axios';

function isLogin(url) {
	return new Promise((resolve, reject) => {
		axios.get(url).then(
			(res) => {
				const { loginStatus } = res.data;
				if (loginStatus == 'fail') {
					resolve(false);
				} else {
					resolve(res.data);
				}
			},
			(err) => {
				reject(false);
			}
		);
	});
}

export default isLogin;
