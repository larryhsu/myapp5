function isELementVisable(el) {
	const rect = el.getBoundingClientRect();
	const vWidth = window.innerWidth || document.documentElement.clientWidth;
	const vHeight = window.innerHeight || document.documentElement.clientHeight;

	if (
		rect.right < 0 ||
		rect.left > vWidth ||
		rect.bottom < 0 ||
		rect.top > vHeight
	) {
		return false;
	}

	return true;
}

export default isELementVisable;
