const scrollAction = { x: undefined, y: undefined };

function getScrollDirection() {
	let scrollDirection;

	if (typeof scrollAction.x == undefined) {
		scrollAction.x = window.pageXOffset;
		scrollAction.y = window.pageYOffset;
	}

	const diffX = scrollAction.x - window.pageXOffset;
	const diffY = scrollAction.y - window.pageYOffset;

	if (diffX < 0) {
		scrollDirection = 'right';
	} else if (diffX > 0) {
		scrollDirection = 'left';
	} else if (diffY < 0) {
		scrollDirection = 'down';
	} else if (diffY > 0) {
		scrollDirection = 'up';
	}

	scrollAction.x = window.pageXOffset;
	scrollAction.y = window.pageYOffset;

	return scrollDirection;
}

export default getScrollDirection;
