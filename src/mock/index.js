const Mock = require('mockjs')
const templates = require.context('./template', false, /^(.*\.(js$))[^.]*$/)
let file = []

templates.keys().map(key => {
  file = [...file, templates(key).default]
})

Mock.setup({
  timeout: '200-600'
})

file.map(el => {
  const { url, type, data } = el(Mock.Random)
  Mock.mock(url, type, data)
})