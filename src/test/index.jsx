import React from 'react';

class Test extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      treeObj: []
    }
  }

  handleClick = () => {
    this.state.treeObj[0].highlight = true;
    console.log("changed");
    this.forceUpdate();
  }

  componentDidMount() {
    const treeObj = [];
    for (let i = 0; i < 5; i++) {
      treeObj[i] = {
        h2Name: `item${i}`,
        ele: `item`,
        highlight: false,
      }
    }

    window.addEventListener("click", this.handleClick, false);
   
    this.setState({
      treeObj: treeObj
    });
  }

  render() {
    return (
      <div>
        {
          this.state.treeObj.map((item, index) => {
            const { highlight } = item;
            return (
              <div key={index}>{highlight ? 'true' : 'false'}</div>
            )
          })
        }
      </div>
    )
  }
}


export default Test;