import React, { useEffect } from "react";
import './app.scss';
import { Redirect, Route } from "react-router-dom";
import {
  NavList,
  Blog,
  MdContent,
  BackTop,
  Home,
  Login,
  UserPage,
  Test,
  About,
  SignUp
} from 'Components';

import Draft from 'UserPage/Draft/index.jsx';

// import showBorders from 'reveal-borders';

import { mockjs } from "../../settings.js";

if ((process.env.NODE_ENV !== "production" || process.env.MOCKJS) && mockjs) {
  const context = require.context("../mock", false, /index/);
  context.keys().map(function(key) {
    context(key);
  });
}

function App(props) {
  
  return (
    <>
      <Route path='/' exact> 
        <Redirect to='/blog' />
        {/* <Home /> */}
      </Route> 

      {/* <Route path='/test' exact >
        <Test />
      </Route> */}
      
      <Route path='/blog' exact>
        <NavList />
        <Blog />
        <BackTop />
      </Route>
      
      <Route path='/blog/:uuid'>
        <NavList />
        <MdContent dst='article' />
        <BackTop />
      </Route>

      <Route path='/draft/:uuid'>
        <NavList />
        <MdContent dst='draft' />
        <BackTop />
      </Route>

      <Route path='/user' exact>
        <div>test</div>
      </Route>

      

      <Route path='/user/login' exact>
        <NavList theme={{loginTheme:true}} />
        <Login />
      </Route>
      <Route path='/user/signUp' exact>
        <SignUp />
      </Route>

      <Route path='/userpage/personal' exact >
        <UserPage />
      </Route>

      <Route path='/userpage/write' exact>
        <Draft />
      </Route>
      
    </>
  )
}

export default App;