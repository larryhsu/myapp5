import React, { useState } from "react";
import { useHistory } from "react-router";
import { Input, Button, Space } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { throttle } from "lodash";
import 'animate.css';

import "./modal.scss";

const md5 = require("md5");

function Modal(props) {
	const {
		title = "请输入密码",
		passdConfirm,
		passd
	} = props;

	const history = useHistory();
	const [iPassd, setiPassd] = useState(""); // inputing passd
	const [shakeX, setShakeX] = useState("");

	const handleInputPassd = (e) => {
		const passdVal = md5(e.target.value);
		setShakeX("");
		setiPassd(passdVal);
	}

	const clickSure = () => {
		// 将密码传递到redux中
		if (iPassd == passd) {
			passdConfirm();
		} else {
			setShakeX("animate__shakeX");
		}
	}

	return (
		<div className="modal">
			<div className="mask"></div>
			<div className="window">
				<div className="modal-title">
					{title}
					<img className="close" src={require("Common/svgs/x.svg")} onClick={() => history.go(-1)} />
				</div>

				<div className={`password animate__animated ${shakeX}`}>
					<Input.Password
						placeholder="input password"
						iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
						onChange={throttle(handleInputPassd, 500)}
					/>
				</div>

				<div className="control">
					<Space>
						<Button size="large" onClick={() => history.go(-1)}> 取消 </Button>
						<Button size="large" type="primary" onClick={clickSure} > 确认 </Button>
					</Space>
				</div>
			</div>
		</div>
	);
}

export default Modal;
