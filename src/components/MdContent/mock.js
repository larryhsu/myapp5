const text = `<h2 id="防抖">防抖</h2>
<blockquote>
  <p>在事件被触发n秒后再执行回调，如果在这n秒内又被触发，则重新计时。</p>
</blockquote>
<pre><code class="language-javascript">function deBounce (fun, delay) {
  return function () {
    var args = arguments;
    clearTimeout(fun.id);
    fun.id = setTimeout(() =&gt; {
        fun.apply(this, args);
    }, delay)
  }
}</code></pre>
<h2 id="防抖测试">防抖测试</h2>
<p>假设有个input标签，其id为inp</p>
<pre><code class="language-html">&lt;input id='inp' &gt;</code></pre>
<pre><code class="language-javascript">function deBounce(fun, delay) {
  return function () {
    var args = arguments;
    clearTimeout(fun.id);
    fun.id = setTimeout(() =&gt; {
        fun.apply(this, args);
    }, delay);
  }
}


function testFn () {
  console.log(this.value);
}


var inp = document.getElementById('inp');
inp.addEventListener('input', deBounce(testFn, 1000));</code></pre>
<p>这样就可以在input输入事件结束1秒后在控制台输出其中的内容</p>
<h2 id="节流">节流</h2>
<blockquote>
  <p>规定在一个单位时间内，只能触发一次函数。如果这个单位时间内触发多次函数，只有一次生效。</p>
</blockquote>
<p>有两种实现的方法，一种是使用时间戳，还有一种是使用定时器</p>
<h3 id="使用时间戳">使用时间戳</h3>
<p>当触发事件的时候，取出当前的时间戳，然后减去之前的时间戳(最一开始值设为 0 )，如果大于设置的时间周期，就执行函数，然后更新时间戳为当前的时间戳，如果小于，就不执行。</p>
<pre><code class="language-javascript">function throttle(fun, delay) {
  var last = 0;
  return function () {
    var args = arguments;
    // new Date()参与运算会自动转换为毫秒数
    var now = +new Date();
    // 时间间隔大于延迟时间才执行
    if (now - last &gt; delay) {
      fun.apply(this, args);
      last = now;
    }
  }
}</code></pre>
<h3 id="使用定时器">使用定时器</h3>
<p>当触发事件的时候，设置一个定时器，再触发事件的时候，如果定时器存在，就不执行，直到定时器执行，然后执行函数，清空定时器，这样就可以设置下个定时器。</p>
<pre><code class="language-javascript">function throttle(fun, delay) {
  var time;
  return function () {
    var args = arguments;
    if (!time) {
      time = setTimeout(() =&gt; {
        time = null;
        fun.apply(this, args);
      }, delay);
    }
  }
}</code></pre>
<h2 id="节流测试">节流测试</h2>
<p>假设有个input标签，其id为inp</p>
<pre><code class="language-html">&lt;input id='inp' &gt;</code></pre>
<pre><code class="language-javascript">function throttle(fun, delay) {
  var last = 0;
  return function () {
    var args = arguments;
    // new Date()参与运算会自动转换为毫秒数
    var now = +new Date();
    // 时间间隔大于延迟时间才执行
    if (now - last &gt; delay) {
      fun.apply(this, args);
      last = now;
    }
  }
}


function testFn () {
  console.log(this.value);
}


var inp = document.getElementById('inp');
inp.addEventListener('input', throttle(testFn, 1000));</code></pre>
<p>这样就可以在input输入框输入数据时，每隔1秒都会在控制台输出其中的内容。</p>
<p>比较上面实现节流的两种方法：</p>
<ul>
  <li>第一种事件会立刻执行，第二种事件会在 n 秒后第一次执行。</li>
  <li>第一种事件停止触发后没有办法再执行事件，第二种事件停止触发后依然会再执行一次事件。</li>
</ul>`

export default text;
