import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import 'highlight.js/styles/github.css';
import { get, throttle } from 'lodash';

import {
  fetchData,
  reCalcId,
  getScrollDirection
} from 'Common/methods';
import './mdContent.scss';
import 'Common/styles/markdown.scss';
import BlogSk from '../Blog/blogSK/blog-sk';
import Modal from './passdModal/index.jsx';

const CRITICAL_LOW = 90;
const CRITICAL_DOWN = 100;
const CRITICAL_UP = 150;

function MdContent(props) {
  const params = useParams();
  const uuid = params.uuid; // 文章id
  const [detail, setDetail] = useState('');
  const [title, setTitle] = useState('');
  const [passd, setPassd] = useState('');
  const [locked, setLocked] = useState(true);
  const [author, setAuthor] = useState('');
  const [mdTime, setMdTime] = useState("");
  const [cate, setCate] = useState("");
  const [treeObj, setTreeObj] = useState([]);
  const [refresh, setRefresh] = useState(false);
  const [loading, setLoading] = useState(true);
  const flatTree = [];

  const url = `/api/${props.dst}/${reCalcId(uuid)}`;

  // fetch data from server
  useEffect(() => {
    fetchData(url).then(res => {
      const {
        title,
        htmDetail,  // marked at server
        mdTime,
        name,
        cate,
        passd,
        sign,
        info,
        authority
      } = get(res, 'data', {});
      setTitle(title);
      setAuthor(name);
      setMdTime(mdTime);
      setCate(cate);
      setDetail(htmDetail);

      if (!passd) {
        setLocked(false);
      } else {
        setLocked(true);
        setPassd(passd);
      }
      setLoading(false);
    }).catch(err => {
      console.log('获取文章信息时发生错误', err);
    });

    const backTop = () => { window.scrollTo(0, 0) }
    window.addEventListener('unload', backTop, false);
    return () => {
      window.removeEventListener('unload', backTop);
    }
  }, []);

  // generate content
  useEffect(() => {
    const tmpTreeObj = [];
    // start while unlocked
    if (!loading && !locked) {
      const h2Tags = [...document.getElementsByTagName("h2")];
      for (let i = 0; i < h2Tags.length; i++) {
        const item = h2Tags[i];
        tmpTreeObj[i] = {
          h2Name: item.innerText,
          ele: item,
          highlight: i == 0 ? true : false,
          h3Child: []
        }

        let next = item.nextElementSibling;
        while (next && next.nodeName != "H2") {
          if (next.nodeName == "H3") {
            tmpTreeObj[i].h3Child.push({
              h3Name: next.innerText,
              ele: next,
              highlight: false
            });
          }
          next = next.nextElementSibling;
        }
      }
      setTreeObj(tmpTreeObj);
    }
  }, [loading, locked]);


  // 点击切换到对应的目录
  const handleClick = (myItem) => {
    const top = myItem.ele.getBoundingClientRect().top;
    const docTop = document.documentElement.scrollTop || document.body.scrollTop;
    window.scrollTo({ top: docTop + top - CRITICAL_LOW, behavior: "smooth" });
  }

  // force update
  useEffect(() => {
    refresh && setRefresh(false);
  }, [refresh])

  // highlight content while scrolling
  useEffect(() => {
    const handleScroll = () => {
      // 目录高亮Rule1: 有且只有一个目录在高亮
      // 判断滑动的方向，分为向上滑动和向下滑动
      const direction = getScrollDirection();
      // init first
      flatTree.forEach(item => item.highlight = false);

      if (direction == "down") {
        for (let i = flatTree.length - 1; i >= 0; i--) {
          const item = flatTree[i];
          // console.log(item);
          if (item.ele.getBoundingClientRect().top < CRITICAL_DOWN) {
            item.highlight = true;
            setRefresh(true);
            break;
          }
        }
      } else if (direction == "up") {
        for (let i = 0; i < flatTree.length; i++) {
          let j = i - 1;
          if (j < 0) j = 0;
          const item = flatTree[i];
          const prev = flatTree[j];
          if (item.ele.getBoundingClientRect().top > CRITICAL_UP) {
            prev.highlight = true;
            setRefresh(true);
            break;
          }
        }
      }
    }

    if (treeObj.length && !loading && !locked) {
      for (let i = 0; i < treeObj.length; i++) {
        const { h3Child } = treeObj[i];
        flatTree.push(treeObj[i]);
        if (h3Child.length) {
          for (let j = 0; j < h3Child.length; j++) {
            flatTree.push(h3Child[j]);
          }
        }
      }
      window.addEventListener("scroll", handleScroll, false);
    }

    return () => {
      window.removeEventListener("scroll", handleScroll, false);
    }
  }, [treeObj, loading, locked]);

  return (
    <React.Fragment>
      {
        loading ? (
          <div className='md-content-wrap'>
            <div className='md-content'>
              <BlogSk />
            </div>
          </div>
        ) :
          !locked ? (
            <div className='md-content-wrap'>
              <div className='md-content'>
                {/* 左边文章区域 */}
                <div className='md-left'>
                  <div className='md-title'> {title} </div>
                  <div className="md-author">作者：{author}</div>
                  <div className="md-cate">分类：{cate}</div>

                  <div className="md-time">更新时间：{mdTime}</div>
                  <div className='md-details md'
                    dangerouslySetInnerHTML={{ __html: detail }}
                  />
                </div>

                {/* 右边目录区域 */}
                <div className='md-right'>
                  <div className='md-directory'> 目录 </div>
                  {
                    treeObj.map((item, index) => {
                      const { h2Name } = item;
                      const len = item.h3Child.length;
                      // 目录高亮Rule2: 当有子目录时，父目录不被高亮(h3Child.length)
                      return (
                        <div className="tree-item" key={index}>
                          <p className={item.highlight ? `h2-tree-light` : `h2-tree`}
                            onClick={() => handleClick(item)}>
                            <span className="square-dot" /><span>{h2Name}</span>
                          </p>
                          {
                            item.h3Child.map((itm, idx) => {
                              const { h3Name } = itm;
                              return (
                                <p className={len && itm.highlight ? 'h3-tree-light' : 'h3-tree'}
                                  key={idx} onClick={() => handleClick(itm)}>
                                  <span className="circle-dot" /><span>{h3Name}</span>
                                </p>
                              )
                            })
                          }
                        </div>
                      )
                    })
                  }
                </div>
              </div>
            </div>
          ) : <Modal passdConfirm={() => setLocked(false)} passd={passd} />
      }
    </React.Fragment>

  )
}

export default MdContent;