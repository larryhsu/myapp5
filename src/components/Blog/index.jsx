import React, { useState, useEffect, useRef } from 'react'
import PostCards from './PostCards/index.jsx'
import { connect } from 'react-redux'
import {
  browserInfo,
  deBounce
} from 'Common/methods'
import { getBlogList } from 'Redux/action/actionCreator'
import './blog.scss'
import BlogSK from './blogSK/blog-sk';

const url = '/api/pages';
const pageHeight = browserInfo.pageHeight;

let mapStateToProps = state => {
  return {
    list: state.postItem.item_list,
    searchList: state.postItem.search_list,
    hasData: state.postItem.hasData,
  }
}

let mapDispatchToPorps = dispatch => ({
  myDps: action => dispatch(action)
})


function Blog(props) {
  const { myDps, list, searchList, hasData } = props;
  console.log('blog list===>', list);
  const cp = useRef(1);

  const handleScroll = () => {
    if (hasData) {
      // 当服务端有数据时才会进行下面的处理
      let bottom = document.getElementsByClassName('blog')[0]
      ?.getBoundingClientRect().bottom;
      if (bottom <= pageHeight) {
        cp.current += 1
        const opt = {
          pages: cp.current,
          eachPageNumber: 10
        }

        myDps(getBlogList(url, 'post', opt))
      }
    }
  }

  // 第二个参数是做比对
  useEffect(() => {
    const opt = {
      pages: 1, // 初始化的时候都是1
      eachPageNumber: 10
    }

    // 初次获取页面数据
    myDps(getBlogList(url, 'post', opt));

    window.addEventListener('scroll', deBounce(handleScroll, 500), false);
    return function () {
      window.removeEventListener('scroll', deBounce(handleScroll, 500));
    }
  }, []);

  return (
    <div className='blog'>
      <div className='blog-left'>
        <div className='render-list'>
          {
            (searchList.length ? searchList : list).map((item, index) => {
              return <PostCards {...item} key={index} />
            })
          }
          {
            // 加载中的动画
            hasData ? <BlogSK /> : ''
          }
        </div>
      </div>

      <div className='blog-right' style={{ height: `${pageHeight}px` }}>
        <div className='blog-declare'>
          <p>苏ICP备20008705号</p>
        </div>
      </div>
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToPorps)(Blog)