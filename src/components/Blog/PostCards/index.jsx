import React from 'react';
import './postcard.scss'
import { filterContent, calcNewId } from 'Common/methods'
import { useHistory, useLocation, Link } from 'react-router-dom';

function PostCard(props) {
  const { pathname } = useLocation();
  const {
    author_id,
    cate,
    detail,
    id,
    passd,
    time,
    title
  } = props;

  return (
    <div className='post-card'>
      <div className='card-name'>
        <span className='card-title'> {title} </span>
      </div>
      <div className='card-time'>
        <img align='middle' alt='loss' src={require('Common/imgs/calendar.png')} />
        <span> { time.substring(0, 10) } </span>
      </div>
      <div className='card-content'>
        {passd ? null : filterContent(detail)}...

        <Link to={{
          pathname: `${pathname}/${calcNewId(id)}`,
          state: {
            // author_id: author_id,
            // cate: cate,
            // detail: detail,
            // passd: passd,
            // time: time,
            // title: title,
            // id: id,
          },
        }} >
          <span> 阅读全文 </span>
        </Link>
      </div>
      <div className='card-inter'>
        {/* TODO:分享功能，获取博文的url然后放到剪切板中，提示用户以获取url，去粘贴吧 */}
        <span className='card-agree'> 分享 </span>
      </div>
    </div>
  )
}

export default PostCard;