import React from 'react';
import { useHistory } from "react-router-dom";

import AboutUI from './AboutUI';
import RotateCircle from './RotateCircle';
import SiteIcon from './SiteIcon';
import SvgLarry from './SvgLarry';


function About(props) {
  const history = useHistory();

  const handleClick = () => history.push('./blog');
  const mouseEnter = (ele) => {
    const dot = document.querySelector(ele);
    dot.style.transition = 'r 0.4s ease-in';
    dot.setAttribute('r', '16');
  };

  const mouseLeave = (ele) => {
    const dot = document.querySelector(ele);
    dot.style.transition = 'r 0.2s ease-in';
    dot.setAttribute('r', '10');
  };
  return (
    <AboutUI>
      <RotateCircle 
        handleClick={handleClick}
        mouseEnter={mouseEnter} 
        mouseLeave={mouseLeave}
      />
      <SiteIcon />
      <SvgLarry />
    </AboutUI>
  )
}

export default About;