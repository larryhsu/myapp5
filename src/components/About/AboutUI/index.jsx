import React from 'react';
import './about.scss';


function AboutUI(props) {

  return (
    <div className="about">
      <div className="center-circle">
        { props.children[0] }
        <div className='divide-line' />
        <div className='find-me'>
          { props.children[1] }
        </div>
        <div className='website-declare'>
          <p>Copyright 2021 © larryhsu.com </p>
          <p>Designed and developed by</p>
          <div className='svg-animate-wrap'>
            { props.children[2] }
          </div>
        </div>
      </div>
    </div>
  )
}

export default AboutUI;