import React from 'react';  

import './rotate-circle.scss';


function RotateCircle(props) {
  const {
    mouseEnter,
    mouseLeave,
    handleClick,
  } = props;
  
  return (
    <svg
      className="rotate-circle"
      onMouseEnter={() => mouseEnter('.dot-circle')}
      onMouseLeave={() => mouseLeave('.dot-circle')}
      onClick={handleClick}
    >
      <path
        id="hsuCircle"
        d="M20 100a80 80 0 1 0 160 0a80 80 0 1 0 -160 0z"
      />
      <text>
        <textPath href="#hsuCircle">
          Click here and you will return to the home page!
        </textPath>
      </text>
      <circle className="dot-circle" cx="100" cy="100" r="10" />
    </svg>
  )
}

export default RotateCircle;