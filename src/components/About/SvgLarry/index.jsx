import React from 'react';
import './svg-larry.scss';

function SvgLarry(props) {
  return (
    <svg version="1.1" className="svg-animate-larry">
      <symbol id="larry">
        <text x="10" y="20" className="text">
          Larry Hsu
        </text>
      </symbol>
      <g>
        <use xlinkHref="#larry" className="use-text" />
        <use xlinkHref="#larry" className="use-text" />
        <use xlinkHref="#larry" className="use-text" />
        <use xlinkHref="#larry" className="use-text" />
        <use xlinkHref="#larry" className="use-text" />
      </g>
    </svg>
  )
}

export default SvgLarry;