import React, { useState } from 'react';
import { 
  ZhihuOutlined, 
  WechatOutlined, 
  GithubOutlined,
  WeiboOutlined,
  TwitterOutlined
} from '@ant-design/icons';
import { Button } from 'antd';

import './site-icon.scss';
import { jumpPage } from 'Common/methods';

const zhi_hu = 'https://www.zhihu.com/people/larry_hsu';
const git_hub = 'https://github.com/larry-hsu';
const wei_bo = 'https://www.weibo.com/larryhsu';
const twitter = 'https://twitter.com/ConstQu';

function SiteIcon () {
  const [showQrCode ,setShowQrCode] = useState(false);

  return (
    <>
      <Button type='text' icon={<ZhihuOutlined />} onClick={() => jumpPage(zhi_hu)} />
      <Button type='text' icon={<WechatOutlined />} 
        onMouseEnter={() => setShowQrCode(true)}
        onMouseLeave={() => setShowQrCode(false)}
      > 
        {
          showQrCode ? <div className='qr-code' /> : null
        }
      </Button>
      <Button type='text' icon={<GithubOutlined />} onClick={() => jumpPage(git_hub)} />
      <Button type='text' icon={<WeiboOutlined />} onClick={() => jumpPage(wei_bo)} />
      <Button type='text' icon={<TwitterOutlined />} onClick={() => jumpPage(twitter)} />
    </>
  )
}

export default SiteIcon;