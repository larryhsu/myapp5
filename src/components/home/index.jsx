import { throttle } from 'lodash';
import React, { useState, useEffect, useRef } from 'react';
import Typed from 'typed.js';
import { browserInfo, deBounce } from 'Common/methods';
import { useHistory, Link } from 'react-router-dom';
import './home.scss';

const whiteHsu = require('./img/hsu-white.png');

let isBasy = false;

function Home () {
  const mask = useRef();
  const home = useRef();
  const bgMask = useRef();
  const [clicked, setClicked] = useState(false);
  const [pos, setPos] = useState({
    posX:50,
    posY:50,
  });

  const {
    pageHeight,
    pageWidth
  } = browserInfo;

  const history = useHistory();

  useEffect(() => {
    mask.current.style.borderWidth = `0 ${pageHeight / 2}px ${pageHeight}px 0`;
    var typed = new Typed('.hm-text', {
      strings: [
        `<p class='hm-time'>一月 23, 2020 </p>
        <p class='hm-title'>要么孤独，要么庸俗</p>
        <p class='hm-summary'>愿所有的美好如约而至，愿所有的黑暗都能看到希望，我们都能微笑前行，人生没有完美，有些遗憾才美...</p>`,
      ],
      stringsElement: null,
      smartBackspace: true,
      showCursor: false,
      typeSpeed: 40,
    });
  }, []);

  const handleClick =(e) => {
    setClicked(clicked ? false : true);
    if (clicked) {
      bgMask.current.classList.remove('goDown');
      bgMask.current.classList.add('goUp');
    } else {
      bgMask.current.classList.remove('goUp');
      bgMask.current.classList.add('goDown');
    }
    const text = e.target.innerText;
    switch (text) {
      case 'Article':
        history.push('/blog');
        break;
      case 'About':
        history.push('/About');
        break;
      case 'Write':
        history.push('/user/write'); // TODO:这里需要登陆
        break;
      default:
        break;
    }
  }

  // const handleMouseMove = (e) => {
  //   // e.persist();
  //   console.log('hsu mouseMoving');
  //   console.log(pos);
  //   // throttleMove(e);
  // }

  // const handleMouseEnter = e => {
  //   console.log('hsu mouseEnter');
  //   let enterX = e.clientX;
  //   let enterY = e.clientY;

  //   home.current.addEventListener('mousemove', (e) => {
  //     // currX = [0, 1]
  //     let currX = Math.abs((e.clientX - enterX)) / pageWidth; // 当前鼠标在的位置减去进入的位置
  //     let currY = Math.abs((e.clientY - enterY)) / pageHeight;
  //     // currX = [-10, 10]
  //     currX = currX * 20;
  //     currY = currY * 10;
  //     console.log('currX===>', currX);
      
  //     // home.current.style['background-position'] = `${50 + currX}% ${50 + currY}%`;
  //   }, false);
  // }

  // const throttleMove = throttle(e => {
  //   const {
  //     pageX,
  //     pageY
  //   } = e;

  //   let position = getComputedStyle(home.current)
  //     .getPropertyValue('background-position');
  //   position = position.split(' ');

  //   // pageX / pageWidth = [0, 1], pageY / pageHeight = [0, 1];
  //   // deltaX = [-1, 1]
    
  //   const deltaX = 2 * (pageX / pageWidth) - 1;
  //   const deltaY = 2 * (pageY / pageHeight) - 1;

  //   const delta = {
  //     deltaX, // : 50 + deltaX * 20,
  //     deltaY // : 50 + deltaY * 5,
  //   }

  //   const oldPos = {
  //     posX: parseFloat(position[0]),
  //     posY: parseFloat(position[1]),
  //   }
    
  //   moveBgPostion(oldPos, delta);
  // }, 100);

  // const moveBgPostion = (oldPos, delta) => {
  //   const {
  //     deltaX,
  //     deltaY
  //   } = delta;

  //   let {
  //     posX,
  //     posY
  //   } = oldPos;

  //   const newPos = {
  //     posX: 50 + deltaX * 20,
  //     posY: 50 + deltaY * 5,
  //   }

  //   let posXtoChange = posX;
  //   let animateId;
  //   let diffPosX = Math.abs(newPos.posX - posX);
  //   console.log([diffPosX]);

  //   function moveAnimate() {
  //     if(diffPosX > 0.1) {
  //       home.current.style.backgroundPositionX = `${posXtoChange}%`;
  //     }

  //     // if(deltaX > 0) {
  //     //   posXtoChange = posXtoChange + 0.5;
  //     //   if(posXtoChange < newPos.posX) {
  //     //     home.current.style.backgroundPositionX = `${posXtoChange}%`;
  //     //   }
  //     // } else {
  //     //   posXtoChange = posXtoChange - 0.5;
  //     //   if(posXtoChange > newPos.posX) {
  //     //     home.current.style.backgroundPositionX = `${posXtoChange}%`;
  //     //   }
  //     // }
      
  //     animateId = setTimeout(moveAnimate, 10);
  //   } 

  //   moveAnimate();
  // }

  // const handleMouseMove = e => {
  //   const newPos = {
  //     posX: (e.clientX / pageWidth * 100),
  //     posY: (e.clientX / pageHeight * 100)
  //   }
  //   let currX = 50;
  //   function bgMove() {
  //     if(newPos.posX > currX) {
  //       home.current.style['background-position-x'] = `${currX++}%`;
  //       setTimeout(bgMove, 10);
  //     } else if(newPos.posX < currX) {
  //       home.current.style['background-position-x'] = `${currX--}%`;
  //       setTimeout(bgMove, 10);
  //     }
  //   }
  //   bgMove();

  //   // home.current.style['background-position-x'] = `${newPos.posX}%`;
  // }

  const handleMouseOut = e => {
    home.current.style['background-position'] = `center`;
  }

  const handleMouseClick = e => {
    let position = getComputedStyle(home.current)
      .getPropertyValue('background-position');
    position = position.split(' ');
    const prevPos = {
      posX: parseInt(position[0]),
      posY: parseInt(position[1])
    }
    const curr = {
      clientX: parseInt(e.clientX / pageWidth * 100),
      clientY: parseInt(e.clientY / pageHeight * 100)
    }

    if(Math.abs(curr.clientX - prevPos.posX) < 10) {
      return;
    }

    let tmpX = prevPos.posX;
    // 适配鼠标的移动速度
    let speed = Math.max(Math.abs(prevPos.posX - curr.clientX) / 20, 1);
    console.log('speed', speed);

    function bgMove() {
      if(tmpX < curr.clientX - speed) {
        tmpX = tmpX + speed;
        home.current.style['background-position-x'] = `${tmpX}%`;
        requestAnimationFrame(bgMove);
      } else if(tmpX > curr.clientX + speed) {
        tmpX = tmpX - speed;
        home.current.style['background-position-x'] = `${tmpX}%`;
        requestAnimationFrame(bgMove);
      }
      // console.log(tmpX);
    }
    bgMove();
  }

  const handleMouseMove = e => {
    e.persist();
    throttleMove(e);
  }

  const throttleMove = throttle(e => {
    handleMouseClick(e);
  }, 10);

  return (
    <div className='home'
      onMouseMove={e => handleMouseMove(e)}
      // onMouseOut={e => handleMouseOut(e)}
      // onClick = {e => handleMouseClick(e)}
      ref={home}
    >
      <div ref={mask} className='hm-mask' />
      <div className='hm-text-wrap'>
        <span className='hm-text' />
      </div>

      <div className='hm-flag' style={{
        background:`url(${whiteHsu})`
      }} />

      <div className='hm-navbar-mask'
        ref={bgMask}
        onClick={(e) => handleClick(e)}
      >
        <div className='hm-navbar'>
          <p>Article</p>
          <p>Write</p>
          <p>About</p>
        </div>
      </div>
    </div>
  );
}

export default Home;
