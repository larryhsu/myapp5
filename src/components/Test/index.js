import React, { useState, useEffect } from 'react';
import './test.scss';

function Test(props) {
	return (
		<div className="test">
			<div className="wrap">
				<div className="ts opencv">
					<div className="module">视频采集模块</div>
					<div className="module-des">
						使用openc的VideoCapture类调用摄像头采集图像，输出采集后的图像帧序列。
					</div>
				</div>
				<div className="ts object">
					<div className="module">目标检测模块</div>
					<div className="module-des">
						使用Single-YOLOV4检测视频采集模块中的输出图像，输出检测后的目标所在位置。
					</div>
				</div>
				<div className="ts distance">
					<div className="module">距离解算和路径记录模块</div>
					<div className="module-des">
						将移动后的目标位置解算到世界坐标系下的真实距离，判断目标移动的距离，并记录目标向前后左右的移动距离。
					</div>
				</div>
				<div className="ts flyfly">
					<div className="module">控制模块</div>
					<div className="module-des">
						使用距离解算和路径记录模块中的数据，控制无人机向目标移动的相同的方向进行移动。
					</div>
				</div>
			</div>
		</div>
	);
}

export default Test;
