import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { AcType } from 'Redux/action'
import { fetchData, isLogin as checkLogin } from 'Common/methods';
import './login.scss';
import { Button, Input } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import 'animate.css';


const crypto = require('crypto');

function Login(props) {
	const history = useHistory();
	const dispatch = useDispatch();
	const isLogin = useSelector(state => state.sign.isLogin);
	const [shakeX, setShakeX] = useState("");

	// 本地状态用来快速判断登陆状态方便跳转

	const handleLogin = () => {
		const name = document.getElementById('user-name').value;
		const hash = crypto.createHash('md5');
		const pass = hash
			.update(document.getElementById('user-pass').value)
			.digest('base64');
		// begin to login and save login status in cookie
		const dead_line = new Promise(resolve => {
			setTimeout(() => {
				resolve('time out');
			}, 3000);
		})

		Promise.race([
			fetchData('/api/signIn', 'post', { name, pass }), dead_line
		]).then(res => {
			console.log(res);
			if (res == 'time out') {
				setShakeX("animate__shakeX");
				// save login status to redux
				dispatch({ type: AcType.SIN_FAIL });
			} else if (res.data) {
				const action = res.data.action;
				if (action == 'success') {
					// save login status to redux
					dispatch({ type: AcType.SIN_SUC, data: res.data });
					history.push('/userpage/personal');
				} else if (action == 'fail') {
					setShakeX("animate__shakeX");
					// save login status to redux
					dispatch({ type: AcType.SIN_FAIL });
				}
			}
		}).catch(err => {
			setShakeX("animate__shakeX");
			// save login status to redux
			dispatch({ type: AcType.SIN_FAIL });
		});
	};

	const handleInput = () => {
		setShakeX("");
	}

	useEffect(() => {
		if (!isLogin) {
			checkLogin('/api/isLogin').then(usrBack => {
				// 登陆失败时usr为false，成功时包含usr信息
				if (usrBack) {
					dispatch({ type: AcType.SIN_SUC, data: usrBack });
					history.push('/userpage/personal');
				}
			});
		} else {
			return history.push('/userpage/personal');
		}
	}, []);

	return (
		<div className="login">
			<div className="login-inner">
				<div className="login-title">
					登入
				</div>

				<p className="login-hr" />

				<div className="login-dialog">
					<span className="span"> 用户名: </span>
					<Input
						id="user-name"
						placeholder="user account"
						className={`input animate__animated ${shakeX}`}
						suffix={<UserOutlined />}
						onChange={handleInput}
					/>
					<br />
					<br />
					<span className="span"> 密码: </span>
					<Input.Password
						id="user-pass"
						className={`input animate__animated ${shakeX}`}
						placeholder="input password"
						onChange={handleInput}
					/>

					<div className="login-btn">
						<Button type="primary" onClick={handleLogin} danger={shakeX == 'animate__shakeX' ? true : false}>
							{shakeX == 'animate__shakeX' ? '登录名或密码错误！' : '登入'}
						</Button>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Login;
