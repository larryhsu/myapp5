import React, { useEffect } from 'react';
import './navbar.scss';
import { Link, useLocation, useHistory } from "react-router-dom";
import { get } from 'lodash';
import { Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { AcType } from 'Redux/action';

import 'hover.css';
import { fetchData, deBounce } from 'Common/methods';

const searchAPI = '/api/search';

function handleClick(history) {
  history.push('/write/new/draft');
}

function NavBar(props) {
  const {
    theme
  } = props;

  const loginTheme = get(theme, 'loginTheme', false);
  const top = useSelector(state => state.top.top);

  const dispatch = useDispatch();

  const location = useLocation()
  const history = useHistory()
  let path = location.pathname;

  const jumpToSignPage = () => {
    // TODO: user's name and password
    history.push('/user/login');
  }

  const jumpToSignUpPage = () => {
    history.push('/user/signUp');
  }

  const handleSearch = (title) => {
    // 查找数据库
    if (title) {
      console.log(title);
      fetchData(searchAPI, 'post', { title })
        .then(res => {
          try {
            const data = res.data;
            if (data.length) {
              console.log(data);
              // 将搜索数据保存到redux中，然后跳到搜索页面使用这个数据进行展示。
              dispatch({ type: AcType.SAVE_SEARCH_DATA, data: data, hasData: false });
              // history.push('/SearchPage');
            } else {
              alert('find nothing!');
            }
          } catch (err) {
            console.error('搜寻时出现了错误！');
          }
        }
        ).catch(err => {
          console.error('搜索操作fetchData出现错误！', err);
        })
    } else {
      alert('please input some search text!');
    }
  }
  const handleChange = e => {
    const value = e.target.value;
    if (!value) {
      // 当输入框中没有文字时，使用原有的博文列表数据进行渲染，并让hasData为true方便触底加载。
      dispatch({ type: AcType.SAVE_SEARCH_DATA, data: [], hasData: true })
    }
  }

  const handleFocus = (e) => {
    // TODO:change width;
    const target = e.target;
  }

  return (
    <div className='navbar'>
      <div className='bar-container'>
        <div className='bar-left'>
          <Link to={`/blog`}>
            <div data-to='blog'>
              <img src={require('../../common/svgs/home.svg')} />
              {/* // <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24"
              //   fill="none" stroke="#008b8b" stroke-width="1" stroke-linecap="round"
              //   stroke-linejoin="round">
              //   <path d="M20 9v11a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V9" />
              //   <path d="M9 22V12h6v10M2 10.6L12 2l10 8.6" />
              // </svg> */}
            </div>
          </Link>

        </div>
        
        <div className='bar-right'>
          <Input.Search
            placeholder="搜索"
            onSearch={handleSearch}
            onChange={handleChange}
            enterButton
            className='bar-search-input'
            style={{ width: 300 }}
            onFocus={(e) => handleFocus(e)}
          />
          <Link to={`/user/login`}>
            <div data-to='login'>
              <img src={require('../../common/svgs/user.svg')} />
            </div>
          </Link>
        </div>
        
      </div>
    </div >
  )
}

export default NavBar;