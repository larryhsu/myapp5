import React from 'react';
import './draft.scss';

function UserDraft(props) {
  return (
    <div className='user-draft'>
      user's draft
    </div>
  )
}

export default UserDraft;