import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import marked from 'marked';
import hljs from 'highlight.js';
import 'highlight.js/styles/github.css';
import { useHistory } from 'react-router-dom';
import { Input } from 'antd';

import './draft.scss';
import 'Common/styles/markdown.scss';
import { AcType } from 'Redux/action';
import {
  deBounce
} from 'Common/methods';
import { fetchData, isLogin as checkLogin } from 'Common/methods';

// TODO: get all category from back end
let allCate = ['JavaScript', 'CSS', 'Client', 'Server','HTTP', 'Webpack', 'Essay', 'Algorithm'];
const url = '/api/save/article';

function Draft() {
  const [md, setMd] = useState();
  const history = useHistory();
  const [cate, setCate] = useState('');
  const [modal, setModal] = useState({ open: false, text: '', error: false });
  const dispatch = useDispatch();
  const isLogin = useSelector(state => state.sign.isLogin);
  
  !isLogin && history.push(`/user/login`);

  const handleKeyDown = () => {
    var txt = document.getElementById('text')
    setMd(marked(txt.innerText))
    highlightCode()
  }

  const highlightCode = () => {
    const preEl = document.querySelectorAll('pre')
    preEl.forEach((el) => {
      hljs.highlightBlock(el)
    })
  }

  const clickToPub = () => {
    var arr = document.getElementById('arrow')
    var panel = document.getElementById('panel')
    // 切换箭头的朝向
    if (arr.className === 'down-arrow') {
      arr.className = 'up-arrow'
      panel.style.display = 'block'
    } else if (arr.className === 'up-arrow') {
      arr.className = 'down-arrow'
      panel.style.display = 'none'
    }
  }

  const comfirmPub = async () => {
    // last confirm to publish 
    if (cate) {
      // site_draft--->title
      const title = document.getElementById('pub-title').innerText;
      // site_draft--->detail
      const detail = document.getElementById('pre').innerHTML;
      console.log(detail);
      let res;
      try {
        res = await fetchData(url, 'post', { title, detail, cate });
      } catch (err) {
        setModal({ open: true, text: '发生了未知错误！' });
      }
      
      if (res.data) {
        // TODO:publish a success modal 
        setModal({ open: true, text: '文章发布成功，等待审核通过后将自动发布！' });
      } else {
        setModal({ open: true, text: '文章发布失败，请重新登陆或者稍后再试！', error: true });
      }
    } else {
      console.log('cannot publish')
    }
  }

  // check login status after render
  checkLogin(`/api/islogin`).then(res => {
    !res && dispatch({ type: AcType.SIN_FAIL});
  }, []);

  const handleTxtScroll = (e) => {
    const target = e.target;
    const pre = document.getElementById('pre');
    pre.scrollTop = target.scrollTop;
  }

  const handleSure = () => {
    // setModal is async
    setModal({ open: false, text: '', error: false });
    if (modal.error) {
      history.push('/user/login');
    }
  }

  return (
    <>
      <div className='draft'>
        <div className='draft-nav'>
          <Input 
            id='pub-title' 
            bordered={false} 
            className='input' 
            placeholder="请输入文章标题..." 
            maxLength='40'
          />
          <div className='draft-nav-right'>
            <div
              className='toggle-btn' onClick={clickToPub} >
              <span> 发布 </span>
              <i id='arrow' className='down-arrow' />
            </div>
            <div id='panel' className='panel'>
              <div className='panel-title'> 发布文章 </div>
              <div className='category-box'> 
                <div className='sub-title'> 
                  分类：
                  <span id='pub-cate'>{cate ? cate : '请选择分类'}</span>
                </div>

                <div className='category-list'>
                  {
                    allCate.map((item, index) => {
                      return (
                        <div
                          className={cate == item ? 'item active' : 'item'}
                          key={index} 
                          onClick={e => setCate(item)}
                        >
                          {item}
                        </div>
                      )
                    })
                  }
                </div>
              </div>
              <button 
                className={cate ? 'publish-btn' : 'publish-btn-grey'} 
                onClick={() => comfirmPub()}
              > 
                确定并发布 
              </button>
            </div>
            <div className='author-avatar' />
          </div>
        </div>

        <div className='draft-details'>
          <div className='draft-write'>
            <div id='text' className='draft-editor' 
              contentEditable
              onKeyUp={deBounce(handleKeyDown, 100)}
              onScroll={e => handleTxtScroll(e)}
            />
          </div>

          <div id='pre'
            className='draft-preview md'
            dangerouslySetInnerHTML={{ __html: md }}
          />
        </div>
      </div>
    </>
  )
}

export default Draft;