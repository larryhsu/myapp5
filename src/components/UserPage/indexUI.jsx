import React from 'react';
import { Layout, Menu, Breadcrumb, Button } from 'antd';
import {
	MenuUnfoldOutlined,
	MenuFoldOutlined,
	HomeOutlined,
	FormOutlined
} from '@ant-design/icons';

import './userpage.scss';

function UserPageUI(props) {
	const {
		collapsed = false,
		toggle,
		selected = '审核',
		jumpToHome,
		setSelected,
		menuTxt,
		clickToWrite
	} = props;
	const { Content, Footer, Sider } = Layout;

	return (
		<Layout style={{ minHeight: '100vh' }}>
			<Sider
				trigger={null}
				collapsible
				collapsed={collapsed}
				width='160'
				style={{ position: 'fixed', height: '100vh' }}
			>
				<div className="logo">
					{
						React.createElement( collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
							{ className: 'trigger', onClick: toggle })
					}
				</div>
				<Menu theme="dark" defaultSelectedKeys={'0'} mode="inline">
					{
						menuTxt.map((item, index) => {
							const { text = '', icon = '' } = item;

							return (
								<Menu.Item
									key={index}
									icon={icon}
									onClick={() => setSelected(text)}
								>
									{ text }
								</Menu.Item>
							);
						})
					}
				</Menu>
			</Sider>

			<Layout className="site-layout" style={{ marginLeft: collapsed ? 80 : 160 }}>
				<Content style={{ margin: '0 16px' }}>
					<div className='site-layout-header'>
						<Breadcrumb style={{ margin: '16px 0' }}>
							<Breadcrumb.Item onClick={jumpToHome} className="site-layout-home">
								<HomeOutlined  /> 博客主页
							</Breadcrumb.Item>
							<Breadcrumb.Item> {selected} </Breadcrumb.Item>
						</Breadcrumb>
						<Button className='write' type='text' onClick={clickToWrite}>
							<FormOutlined /> 写文章
						</Button>
					</div>	
					
					<div className="site-layout-content">
						{
							menuTxt.map(item => {
								const { text, content } = item;
								if (text == selected) {
									return content;
								}
							})
						}
					</div>
				</Content>

				<Footer style={{ textAlign: 'center' }}> larryhsu.com ©2021 Created by Larry Hsu </Footer>
			</Layout>
		</Layout>
	)
}

export default UserPageUI;