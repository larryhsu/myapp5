import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { isNull } from "lodash"
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  AppstoreOutlined,
  HomeOutlined,
  ApartmentOutlined,
  SettingOutlined,
  FileProtectOutlined,
  PushpinOutlined
} from '@ant-design/icons';

import UserAbout from 'UserPage/UserAbout';
import UserArticle from './UserArticle';
import UserDraft from './UserDraft';
import UserColumn from './UserColumn';
import UserApprove from './UserApprove';
import UserPageUI from './indexUI';
import { isLogin as checkLogin } from "Common/methods";
import { checkPermission } from './utils';


function UserPage(props) {
  const history = useHistory();
  const [collapsed, setCollapsed] = useState(false);
  const [selected, setSelected] = useState('审核');
  // 登陆成功时存储的usr信息
  let { usr } = useSelector(state => state.sign);
  // 刷新后从服务端取出的usr信息
  const [usrBp, setUsrBp] = useState(usr);

  const menuTxt = [{
      text: '审核', icon: <ApartmentOutlined />, content: <UserApprove user={usrBp} key="AP" />
    }, {
      text: '草稿', icon: <PushpinOutlined />, content: <UserDraft user={usrBp} key="D" />
    }, {
      text: '文章', icon: <FileProtectOutlined />, content: <UserArticle user={usrBp} key="AR" />
    }, {
      text: '专栏', icon: <AppstoreOutlined />, content: <UserColumn user={usrBp} key="C" />
    }, {
      text: '设置', icon: <SettingOutlined />, content: <UserAbout user={usrBp} key="AB" />
    }];
  
  if (checkPermission(usrBp) && menuTxt.length <= 4) {
    menuTxt.shift();
  }

  // 校验登陆状态
  useEffect(() => {
    if (isNull(usr)) {
      checkLogin('/api/isLogin').then(usrBp => {
        // 登陆失败时usr为false，成功时包含usr信息
        if (!usrBp) {
          // 未登录状态跳转到登陆页
          history.push(`/user/login`);
        } else {
          setUsrBp(usrBp);
        }
      });
    }
  }, []);

  return (
    <UserPageUI
      collapsed={collapsed}
      toggle={() => setCollapsed(!collapsed)}
      selected={selected}
      jumpToHome={() => history.push('/blog')}
      menuTxt = {menuTxt}
      setSelected={setSelected}
      clickToWrite={() => history.push('/userpage/write')}
    />
  )
}

export default UserPage;
