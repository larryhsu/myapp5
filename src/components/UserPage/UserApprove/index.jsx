import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { fetchData } from 'Common/methods';
import UserApproveUI from './UserApproveUI';
import { AcType } from 'Redux/action';

const url_draft = '/api/draft/all/article';
const url_name = '/api/user/name';

let data = [];

function UserApprove(props) {
  const {
    user = {}
  } = props;

  const dispatch = useDispatch();

  useEffect(() => {
    if (!data.length) {
      // get all draft from database;
      fetchData(url_draft).then(res => {
        if (res.status = '200') {
          data = res.data;
          // 使用data中的作者id查询作者名字
          for (let i = 0; i < data.length; i++) {
            const ato_id = data[i].author_id;
            data[i]['key'] = i + 1; // 为每一个data设置key，antd表格要求
            data[i].time = data[i].time.substr(0, 10); // 处理事件格式
            fetchData(
              url_name,
              'post',
              { author_id: ato_id }
            ).then(res => {
              if (res.data) {
                data[i]['username'] = res.data;
                if (i == data.length - 1) {
                  dispatch({ type: AcType.GET_DRAFT_LIST, data: data });
                }
              } else {
                console.error('获取用户姓名发生错误，详细在UserPage/UserApprove/index.jsx');
              }
            });
          }
        }
      });
    }
  }, []);

  return <UserApproveUI />
  
}

export default UserApprove;