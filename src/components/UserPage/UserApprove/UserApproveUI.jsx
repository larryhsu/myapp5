import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { Table, Tag, Space } from 'antd';

import { calcNewId } from 'Common/methods';

import './style.scss';



function UserApproveUI(props) {
  const { Column } = Table;
  const history = useHistory();
  const draft_list = useSelector(state => state.draftList.draft_list);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (draft_list.length) {
      setLoading(false);
    }
  }, [draft_list])

  /**
   * 
   * @param {string} text 当前行的内容
   * @param {string} record 当前行的数据
   * @param {int} index 当前行的索引
   */
  const handleView = (text, record, index) => {
    const { id } = record;
    const newId = calcNewId(id);
    history.push(`/draft/${newId}`); // TODO:需要在新的标签中打开
  }

  const handlePass = (text, record, index) => {

  }

  const handleDeny = (text, record, index) => {

  }

  return (
    <Table dataSource={draft_list} className='UAUI' loading={loading} >
      <Column title="序号" dataIndex="key" key="key" />
      <Column title="文章标题" dataIndex="title" key="title" />
      <Column title="作者昵称" dataIndex="username" key="username" />
      <Column title="文章类别" dataIndex="cate" key="cate" />
      <Column title="发布时间" dataIndex="time" key="time" />
      <Column
        title="文章审核操作"
        key="action"
        render={(text, record, index) => (
          <Space size="middle">
            <a className='view' onClick={() => handleView(text, record, index)}>查看</a>
            <a className='pass' onClick={() => handlePass(text, record, index)}>通过</a>
            <a className='deny' onClick={() => handleDeny(text, record, index)}>忽略</a>
          </Space>
        )}
      />
    </Table>
  )
}

export default UserApproveUI;