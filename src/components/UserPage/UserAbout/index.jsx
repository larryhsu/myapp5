import React, { useState, useEffect } from 'react';
import { Modal } from 'antd';
import UserAboutUI from './UserAboutUI';
import { fetchData } from 'Common/methods';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { AcType } from 'Redux/action'

function UserAbout(props) {
  const { user = {} } = props;
  const [modal, setModal] = useState({ open: false, error: false });
  const [editModal, setEditModal] = useState({ 
    open: false, 
    labelName: '', 
    defaultValue: '',
   });
  const [loading, setLoading] = useState(true);
  const [gender, setGender] = useState('');
  const history = useHistory();
  const dispatch = useDispatch();

  const handleSure = () => {
    setModal({ open: false });
    setEditModal({ open: false });
  }

  const handleCancel = () => {
    setModal({ open: false });
    setEditModal({ open: false });
  }

  const handleEdit = (item) => {
    //TODO: defaultValue change to real value
    setEditModal({ open: true, labelName: item.title, defaultValue: item.content });
  }

  const handleChange = event => {
    setGender(event.target.value);
  }

  const logOut = () => {
    // destory token in cookies 
    fetchData(`/api/singOut`).then(res => {
      const { action } = res.data;
      if (action == 'success') {
        dispatch({ type: AcType.SIN_OUT });
        history.push(`/user/login`);
      }
    });
  }

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, [])

  return (
    <>
      <UserAboutUI 
        user={user}
        loading={loading}
        handleEdit={handleEdit}
        setModal={setModal}
        signOut={logOut}
      />
    </>
  )
}

export default UserAbout;