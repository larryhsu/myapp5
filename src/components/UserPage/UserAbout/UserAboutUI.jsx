import React from 'react';
import './about.scss';
import { Button } from 'antd';
import {
  Card,
  Space,
  Divider,
  Avatar,
  Row,
  Col
} from 'antd';
import { EditOutlined, UserOutlined } from '@ant-design/icons';
import './about.scss';

function UserAboutUI(props) {
  const { 
    user = {},
    setModal,
    handleEdit,
    loading,
    signOut
  } = props;

  const {
    photo = '', name = '', gender = '', birthday = '', email = '', phone = ''
  } = user;

  const basic_info = [
    { title: 'photo', content: <Avatar size={64} icon={<UserOutlined />} /> },
    { title: 'name', content: name },
    { title: 'gender', content: gender },
    { title: 'birthday', content: birthday.substring(0, 10) },
    { title: 'password', content: '********' }
  ];

  const contact_info = [
    { title: 'email', content: email },
    { title: 'phone', content: phone }
  ];

  return (
    <div className='user-about'>
      <div className='user-about-title'> Personal information </div>
      <div className='user-about-sub-title'>
        Basic information you use in the services of this website, such as your name and photo
      </div>
      <Space direction="vertical">
        <Card
          title="Basic information"
          style={{ width: 800, borderRadius: '6px' }}
          loading={loading}
        >
          {
            basic_info.map((item, index) => {
              const { title = '', content = '' } = item;

              return (
                <Row align='middle' key={index}>
                  <Col span={10}> {title} </Col>
                  <Col span={10} style={{fontSize: '18px'}}>
                    { content }
                  </Col>
                  <Col span={4} style={{ textAlign: 'right' }}>
                    <EditOutlined className='user-about-edit' onClick={() => handleEdit(item)} />
                  </Col>
                  { index == 4 ? null : <Divider plain />}
                </Row>
              )
            })
          }
        </Card>
        <Card
          title="Contact information"
          style={{ width: 800, borderRadius: '6px' }}
          loading={loading}
        >
          {
            contact_info.map((item, index) => {
              const { title = '', content = '' } = item;

              return (
                <Row align='middle' key={index}>
                  <Col span={10}> {title} </Col>
                  <Col span={10} style={{fontSize: '18px'}}>
                    { content }
                  </Col>
                  <Col span={4} style={{ textAlign: 'right' }}>
                    <EditOutlined className='user-about-edit' onClick={() => handleEdit(item)} />
                  </Col>
                  { index == 1 ? null : <Divider plain />}
                </Row>
              )
            })
          }
        </Card>
        <Button
          type="primary"
          onClick={signOut}
          block
          className='user-sign-out-btn'
          size='large'
        >
          Log Out
        </Button>
      </Space>
    </div>
  )
}

export default UserAboutUI;