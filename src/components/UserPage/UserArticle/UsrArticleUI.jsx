import React from 'react';
import { FormOutlined } from '@ant-design/icons';

import './article.scss';

function UsrArticleUI(props) {
  return (
    <div className='user-article'>
      user's article
    </div>
  )
}

export default UsrArticleUI;