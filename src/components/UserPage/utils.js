import { get } from 'lodash';

const checkPermission = (user) => {
	const permission = get(user, 'authority', 0);
	if (permission >= 7) {
		return true;
	}
	return false;
};

export { checkPermission };
