import React, {useState} from 'react';
import './edit-modal.scss';
import Button from '@material-ui/core/Button'; // 这个是模态框中的button
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';

function EditModal (props) {
  const {
    open=false,
    onClickCancel,
    onClickSure,
    defaultValue,
    labelName,
    handleChange,
    selected=''
  } = props;

  const [showPassword, setShowPassword] = useState(false);

  const handleSure = (labelName) => {
    console.log(labelName);
    switch(labelName) {
      case 'name':
        
        break;
      case 'gender':
        break;
      case 'birthday':
        break;
      case 'password':
        break;
      case 'email':
        break;
      case 'phone':
        break;
      default:
        break;
    }
    onClickSure();
  }

  return (
    <Dialog style={{ minWidth:'600px'}} open={open}>
      <DialogTitle>
        资料修改
        {
          labelName == 'password'
          ?  <div className='eye-to-see'>
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => setShowPassword(!showPassword)}
                  onMouseDown={() => {}}
                >
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            </div>
          : null
        }
       
      </DialogTitle>
      <DialogContent dividers>
        {
          labelName == 'gender' 
          ? <>
              <InputLabel shrink> gender </InputLabel>
              <Select value={selected || defaultValue} onChange={e => handleChange(e)}>
                <MenuItem value='male'> male </MenuItem>
                <MenuItem value='female'> female </MenuItem>
              </Select>
            </>
          : labelName == 'password'
          ? <> 
            {
              ['old password', 'new password', 'confirm password'].map((item, index) => {
                return (
                  <FormControl key={index} style={{display: 'block', marginBottom: '12px'}}>
                    <InputLabel htmlFor="standard-adornment-password"> { item } </InputLabel>
                    <Input
                      id="standard-adornment-password"
                      fullWidth
                      type={showPassword ? 'text' : 'password'}
                    />
                  </FormControl>
                )
              })
            }
            </> 
          
          : <TextField 
              required 
              type={labelName=='birthday' ? 'date' : ''}
              label={labelName} 
              defaultValue={defaultValue} 
            />
        }
      </DialogContent>
      <DialogActions>
        <Button onClick={onClickCancel}>
          取消
        </Button>
        <Button autoFocus color="primary" onClick={() => handleSure(labelName)}>
          确认
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default EditModal;