import Blog from './Blog';
import NavList from './NavList';
// import Draft from './Draft';
import MdContent from './MdContent';
import BackTop from './backTop';
import Home from './home';
import Login from './Login/index.jsx';
import UserPage from './UserPage';
import About from './About';
import SignUp from './SignUp';
import Test from '../test';

export {
	Blog,
	NavList,
	MdContent,
	BackTop,
	Home,
	Login,
	UserPage,
	About,
	SignUp,
	Test,
};
