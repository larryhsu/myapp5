import React, { useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { AcType } from 'Redux/action'
import './back.scss';

function BackTop(props) {
  const backTop = useRef(null);
  const dispatch = useDispatch();
  let firstTop = true;

  // switch the style of go top icon
  const handleScroll = () => {
    const curr = backTop.current;
    const top = document.body.scrollTop || document.documentElement.scrollTop;
    
    dispatch({type: AcType.SAVE_TOP, top: top});

    if (top < 400) {
      if (!firstTop) { 
        curr.classList.remove('back-top-animate');
        curr.classList.add('top-back-animate');
      }
    } else {
      curr.classList.remove('top-back-animate');
      curr.classList.add('back-top-animate');
      firstTop = false;
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll, false);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    }
  }, []);


  const goBack = () => {
    const top = document.body.scrollTop || document.documentElement.scrollTop;
    const speed = top / 4;
    if (document.body.scrollTop != 0) {
      document.body.scrollTop -= speed;
    } else {
      document.documentElement.scrollTop -= speed;
    }

    top != 0 && requestAnimationFrame(goBack);
  }

  return (
    <div className='back-top'
      onClick={() => requestAnimationFrame(goBack)}
      ref={backTop}
    />
  );
}

export default BackTop;