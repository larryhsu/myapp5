const archiver = require("archiver");
const fs = require("fs");
const pkg = require("../package.json");
const productionName = pkg.name;
const path = require("path");

class ArchiverWebpackPlugin {
  constructor(options = {}) {
    this.output = options.output;
    this.format = options.format || "zip";
    this.archive = options.archive;
  }
  // do archive task after finishing and sealing the compilation
  apply(compiler) {
    const ext = this.format;
    const output = this.output || `${compiler.options.output.path}.${ext}`;
    // afterEmit => async hook
    compiler.plugin("afterEmit", () => {
      const archive = this.archive || archiver(this.format);
      archive.pipe(fs.createWriteStream(output));
      archive.bulk([
        {
          expand: true,
          cwd: path.join(__dirname, "../dist/", productionName),
          src: ["js/**", "img/**", "css/**", "plugin/**", "*.html"],
          dest: productionName
        }
      ]);
      archive.finalize();
    });
  }
}

module.exports = ArchiverWebpackPlugin;
