const merge = require('webpack-merge')
const base = require('./webpack.config.common')


let config

if (process.env.NODE_ENV === 'production') {
  config = require('./webpack.config.production')
} else {
  config = require('./webpack.config.development')
}

module.exports = merge(base, config)
