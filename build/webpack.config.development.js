const webpack = require("webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const path = require("path")

module.exports = {
  entry: {
    // 已经在package.json中直接指定了配置文件，所以这里的路径应该是相对于整个项目来说的
    index: './src/index.jsx'
  },
  output: {
    filename: 'js/[name].bundle.js',
    publicPath: '/assets/' // 资源间接引用的位置
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: "index.html", 
      template: "./src/index.tpl.html",
      inject: "body",
      hash: true
    }),
    new MiniCssExtractPlugin({
      filename: "css/[name].[hash].css",
      chunkFilename: "css/[id].[hash].css"
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    disableHostCheck: true,
    open: true,
    // historyApiFallback: true,
    historyApiFallback:{
      index:'/assets/'
    },
    proxy: {
      '/api/*': {
        target: 'http://127.0.0.1:3000',
        changeOrigin: true,
      },
      // '/': {
      //   target: 'http://127.0.0.1',
      //   secure: false,
      //   bypass: function(req, res, proxyOptions) {
      //     console.log(req)
      //     if (req.headers.accept.indexOf('html') !== -1) {
      //       console.log('Skipping proxy for browser request.');
      //       return '/index.html';
      //     }else if(req.headers.referer.indexOf('user/write') !== -1){
      //       console.log('skip write')
      //       return '/';
      //     }
      //   }
      // }
    }
  },
  devtool: 'source-map'
}