const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HappyPack = require('happypack');
const os = require('os');
const happyThreadPool = HappyPack.ThreadPool({ size: os.cpus().length });
const aliasPath = require('../config').compilerOptions.paths;

module.exports = {
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				use: ['babel-loader', 'ts-loader'],
			},
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				use: 'happypack/loader?id=jsx',
			},
			{
				test: /\.(scss|css)/,
				use: [
					// {
					//   loader: MiniCssExtractPlugin.loader,
					//   options: {
					//     publicPath: "../"
					//   }
					// },
					'style-loader',
					'css-loader',
					'postcss-loader',
					'sass-loader',
					{
						loader: 'sass-resources-loader',
						options: {
							resources: path.resolve(
								__dirname,
								'../src/common/styles/variable.scss'
							),
						},
					},
				],
			},
			{
				test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
				loader: 'url-loader',
				options: {
					esModule: false, // 不加这个属性会将打包为[object Module]
					limit: 1000,
					name: 'img/[name].[hash:7].[ext]',
				},
			},
			{
				test: /\.(woff|ttf|eot｜otf)$/,
				loader: 'file-loader',
				options: {
					esModule: false,
				},
			},
		],
	},

	plugins: [
		new HappyPack({
			id: 'jsx',
			threadPool: happyThreadPool,
			loaders: [
				{
					loader: 'babel-loader',
					exclude: /node_modules/,
					options: {
						cacheDirectory: '.webpack_cache',
					},
				},
			],
		}),
	],

	resolve: {
		modules: ['src', 'node_modules'], // 配置 Webpack 去哪些目录下寻找第三方模块
		extensions: ['.js', '.jsx'], // 在导入语句没带文件后缀时，Webpack 会自动带上后缀后去尝试访问文件是否存在。
		alias: Object.keys(aliasPath).reduce((prev, key) => {
			prev[key] = path.resolve(aliasPath[key][0] + '');
			return prev;
		}, {}),
	},
};
