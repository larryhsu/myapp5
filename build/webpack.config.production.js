const TerserPlugin = require("terser-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ArchiverPlugin = require('./archiverPlugin');

/**
 * chunkFilename 指未被列在 entry 中，却又需要被打包出来的 chunk 文件的名称。
 * 一般来说，这个 chunk 文件指的就是要懒加载的代码。
 */

module.exports = {
  entry: {
    index: './src/index.jsx',
    vendor: [
      'react',
      'redux',
      'react-dom',
      'react-redux',
      'react-router-dom',
      'axios'
    ]
  },
  output: {
    filename: 'js/[name].bundle.[chunkhash].js',
    chunkFilename: 'js/[name].chunk.[chunkhash].js'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        // 提取公共的代码
        commons: {
          name: "vendor",
          chunks: "initial", // 只对入口文件生效
          minChunks: 2
        }
      }
    },
    minimize: true,
    minimizer: [
      new TerserPlugin({

      })
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      // 输出的文件名
      filename: 'index.html',
      // 需求的模版
      template: 'src/index.tpl.html',
      inject: 'body',
      minify: {
        //压缩HTML文件
        removeComments: true,
        collapseWhitespace: true
      }
    }),
    new MiniCssExtractPlugin({
      // 同步代码
      filename: 'css/[name].bundle.[hash].css',
      // 异步代码
      chunkFilename: 'css/[id].bundle.[hash].css'
    }),
    new OptimizeCssAssetsPlugin({}), //压缩css
    new CleanWebpackPlugin(),
    new ArchiverPlugin({
      format: "zip"
    })
  ]
}